<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
		
		</div>
		<ul class="nav navbar-nav">
			
			<li><a href="deleteAccount?userId=${user.id}"  onclick="return confirm('Are you sure you want to delete your account?');">Delete Account</a></li>
		
		</ul >
		<h3 style="text-align: center"><font color="white">	Edit Profile</font></h3>
	</div>
	</nav>
	<div class="container">

		<form action="/updateProfile" method="post" modelAttribute="user">
			<input type="hidden"
					class="form-control" id="id" name="id" value="${user.id}">
			<input type="hidden"
					class="form-control" id="password" name="password" value="${user.password}">
		<input type="hidden"
					class="form-control" id="gender" name="gender" value="${user.gender}">
		
			
			<div class="form-group">
				<label for="firstName">firstName:</label> <input type="text"
					class="form-control" id="firstName" 
					name="firstName" value="${user.firstName} ">
			</div>

			<div class="form-group">
				<label for="lastName">lastName:</label> <input type="text"
					class="form-control" id="lastName"
					 name="lastName"
					value="${user.lastName} ">
			</div>
			<div class="form-group">
				<label for="email">email:</label> <input type="text"
					class="form-control" id="email"
					 name="email"
					value="${user.email} ">
			</div>
			<div class="form-group">
				<label for="phoneNumber">phoneNumber:</label> <input type="text"
					class="form-control" id="phoneNumber"
					 name="phoneNumber"
					value="${user.phoneNumber} ">
			</div>

<div class="form-group">
				 <input type="hidden"
					class="form-control" id="dob"
					 name="dob" value="${user.dob} ">
			</div>
			
			<button type="submit" class="btn btn-default" value="update">Submit</button>
			<a href="http://localhost:8080/getEmail" style="color: #fff">Change
				password</a>
		</form>
	</div>

</body>
</html>