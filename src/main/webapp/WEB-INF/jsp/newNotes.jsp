<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<body>
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			
		</div>
		<h3 style="text-align: center"><font color="white">	New Notes</font></h3>
	</div>
	</nav>
<div class="container">

		<form action="saveNotes" method="post" modelAttribute="notes">
		<div class="form-group">
		<input type="hidden"
					class="form-control" id="userId" value="${userId}" name="user.id">

			<div class="form-group">
				<label for="Subject">Subject:</label> <input type="text"
					class="form-control" id="Subject" placeholder="Enter Subject"
					name="subject">
			</div>

			<div class="form-group">
				<label for="Description">Description:</label> <input type="text"
					class="form-control" id="Description"
					placeholder="Enter Description" name="description">
			</div>

<!-- <div class="form-group">
				<label for="reminderDate">reminderDate:</label> <input type="datetime"
					class="form-control" id="reminderDate"
					name="reminderDate">
			</div> -->
			

			<button type="submit" class="btn btn-default" value="saveNotes">Submit</button>
		</form>
	</div>

</body>
</html>