<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/changePassword.css" / >
	<script type="text/javascript">
	var check = function() {
		  if (document.getElementById('password').value ==
		    document.getElementById('confirm_password').value) {
		    document.getElementById('message').style.color = 'green';
		    document.getElementById('message').innerHTML = 'matching';
		  } else {
		    document.getElementById('message').style.color = 'red';
		    document.getElementById('message').innerHTML = 'not matching';
		  }
		}
	function errormsg() {
		 if (document.getElementById('password').value !=
			    document.getElementById('confirm_password').value){
			 var answer = confirm ("confirm password doesn't match ")
				if (answer)
				window.location="http://localhost:8080/changePassword";
				}
			    }
	
		</script>
<title>Insert title here</title>
</head>
<body>
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			
		</div>
		
	</div>
	</nav>

<form action="savePassword" method="post" modelAttribute="email" modelAttribute="password">
<h2><center>Change password</center></h2>
  <hr>
  <div class="container">
  
    <input type="hidden"
					 id="email" name="email" value="${email}">
    <label for="newpassword"><b> New Password:</b></label>
    <input type="password" placeholder="Enter New Password"  id="password" name="password"  onkeyup='check();'required>
      <br>
    <label for="conpassword"><b> Confirm Password:</b></label>
    <input type="password" placeholder="confirm Password" name="conpassword" id="confirm_password" onkeyup='check();'required>
      <span id='message'></span><br><br>
    <button type="submit" value="savePassword" onclick="errormsg();">save</button><br>
   
  </div>

  
</form>
</body>
</html>