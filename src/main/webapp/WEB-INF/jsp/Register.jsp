<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			
		</div>
		<h3 style="text-align: center"><font color="white">	Register Here</font></h3>
	</div>
	</nav>
	<div class="container">

		<form action="register" method="post" modelAttribute="user">

			<div class="form-group">
				<label for="firstName">FirstName:</label> <input type="text"
					class="form-control" id="firstName"
					placeholder="Enter your firstName" name="firstName">
			</div>
			<div class="form-group">
				<label for="lastName">LastName:</label> <input type="text"
					class="form-control" id="lastName"
					placeholder="Enter your lastName" name="lastName">
			</div>
			<div class="form-group">
				<label for="gender">Gender:</label> <input type="text"
					class="form-control" id="gender" placeholder="Enter your gender"
					name="gender">
			</div>

			<div class="form-group">
				<label for="email">Email:</label> <input type="text"
					class="form-control" id="email" placeholder="Enter email"
					name="email">
			</div>
			<div class="form-group">
				<label for="phoneNumber">phoneNumber:</label> <input type="text"
					class="form-control" id="phoneNumber"
					placeholder="Enter phoneNumber" name="phoneNumber">
			</div>
			<div class="form-group">
				<label for="dob">Date of Birth:</label> <input type="date"
					class="form-control" id="dob"
					 name="dob">
			</div>
			


			<div class="form-group">
				<label for="password">Password:</label> <input type="password"
					class="form-control" id="password" name="password">
			</div>




			<button type="submit" class="btn btn-default" value="register">Submit</button>
		</form>
	</div>

</body>
</html>