package org.aayushi.test.springboot.otp;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class OTPUtility {

public  String generateOTP() {
		
		StringBuilder builder = new StringBuilder();
		Random random = new Random();
		for (int i =1;i<=6;i++) {
			builder.append(random.nextInt(9));
		}
		return builder.toString();
	}
}
