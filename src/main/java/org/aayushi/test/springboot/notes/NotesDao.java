package org.aayushi.test.springboot.notes;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class NotesDao {
	@Autowired
	private NotesRepository notesRepository;

	public List<Notes> getAllNotes(Long userId) {
		return notesRepository.findByUserId(userId);

	}

	public Notes getNotes(Long id) {
		return notesRepository.findOne(id);
	}

	public void addNotes(Notes notes) {
		notesRepository.save(notes);

	}

	public void updateNotes(Notes notes, Long id) {
		Notes findOne = notesRepository.findOne(id);
		if (findOne == null) {
			throw new IllegalArgumentException("Notes doesn't exist with id=" + id);
		}
		notes.setId(id);
		notesRepository.save(notes);

	}

	public void deleteNotes(Long id) {
		notesRepository.delete(id);

	}
	public void deleteALLNotes(Long id) {
		notesRepository.deleteNotes(id);
	}

}
