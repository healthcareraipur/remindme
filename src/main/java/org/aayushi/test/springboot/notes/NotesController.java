package org.aayushi.test.springboot.notes;

import java.util.Date;
import java.util.List;

import org.aayushi.test.springboot.reminder.Reminder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class NotesController {
	@Autowired
	private NotesDao notesDao;

	@RequestMapping(method = RequestMethod.GET, value = "/notes")
	public ModelAndView getAllNotes(@RequestParam Long userId) {
		List<Notes> allNotes = notesDao.getAllNotes(userId);

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("userId", userId);
		modelAndView.setViewName("notes");
		modelAndView.addObject("allNotes", allNotes);
	//	modelAndView.addObject("userId", userId);
		if (!allNotes.isEmpty()) {
			modelAndView.addObject("userId", allNotes.get(0).getUser().getId());
		}
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/newNotes")
	public ModelAndView getNewNotes(@RequestParam Long userId) {

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("newNotes");
		modelAndView.addObject("userId", userId);

		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/deleteNotes")
	public ModelAndView deleteReminder(@RequestParam Long id) {
		Notes notes = notesDao.getNotes(id);
		if (notes == null) {
			throw new IllegalArgumentException("Invalid notes id : " + id);
		}
		notesDao.deleteNotes(id);
		return buildAllNotesModelAndView(notes.getUser().getId());
	}
	@RequestMapping(method = RequestMethod.GET, value = "/getupdateNotes") 
	public ModelAndView updateReminder(@RequestParam Long id) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("updateNotes");
		
		Notes notes = notesDao.getNotes(id);
		modelAndView.addObject("notes", notes);
		modelAndView.addObject("userId", notes.getUser().getId());
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/saveNotes")
	public ModelAndView addNotes(@ModelAttribute("notes") Notes notes) {
		Date date = new Date();
		notes.setDate(date);
		notesDao.addNotes(notes);
		return buildAllNotesModelAndView(notes.getUser().getId());
	}

	@RequestMapping(method = RequestMethod.POST, value = "/updateNotes")
	public ModelAndView updateNotes(@ModelAttribute("notes") Notes notes) {
		Date date = new Date();
		notes.setDate(date);
		notesDao.updateNotes(notes, notes.getId());
		return buildAllNotesModelAndView(notes.getUser().getId());
	}
	
	 
	 

	private ModelAndView buildAllNotesModelAndView(Long userId) {
		ModelAndView modelAndView = new ModelAndView("redirect:" + "/notes");
		modelAndView.addObject("userId", userId);
		return modelAndView;
	}
	
}
