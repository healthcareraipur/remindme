package org.aayushi.test.springboot.sendWish;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.aayushi.test.springboot.reminder.EmailUtility;
import org.aayushi.test.springboot.user.User;
import org.aayushi.test.springboot.user.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class BirthdayWish {
	@Autowired
	private UserDao userdao;
	@Autowired
	private EmailUtility emailUtility;

	// run at 8 AM every day
	@Scheduled(cron = "0 0 8 * * ?")

	public void checkAndSendBirthdayWish() {
		Date currdate = new Date();
		List<User> users = userdao.getAllUser();
       String subject,description;
		Calendar today = Calendar.getInstance();
		Calendar birthDay = Calendar.getInstance();
		for (User user : users) {
			today.setTime(currdate);
			birthDay.setTime(user.getDob());
           subject="hello "+ user.getfirstName();
           description = "many many happy returns of the day";
			if ((today.get(Calendar.MONTH) == birthDay.get(Calendar.MONTH))
					&& (today.get(Calendar.DAY_OF_MONTH) == birthDay.get(Calendar.DAY_OF_MONTH))) {
				emailUtility.sendEmail(user.getEmail(), subject,description);
				System.out.println("happy birthday" + user.getfirstName());

			}
		}
	}
}
