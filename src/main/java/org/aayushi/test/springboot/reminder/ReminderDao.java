package org.aayushi.test.springboot.reminder;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ReminderDao {
	@Autowired
	private ReminderRepository reminderRepository;

	public List<Reminder> getAllReminder(Long userId) {
		return reminderRepository.findByUserId(userId);

	}

	public Reminder getReminder(Long id) {
		return reminderRepository.findOne(id);
	}

	public void addReminder(Reminder reminder) {
		reminderRepository.save(reminder);

	}

	public void updateReminder(Reminder reminder, Long id) {
		Reminder findOne = reminderRepository.findOne(id);
		if (findOne == null) {
			throw new IllegalArgumentException("Reminder doesn't exist with id=" + id);
		}
		reminder.setId(id);
		reminderRepository.save(reminder);

	}

	public void deleteReminder(Long id) {
		reminderRepository.delete(id);

	}

	public List<Reminder> getUnsentReminder() {
		return reminderRepository.findByemailSentFalse();
	}

	public void updateEmailSent(Long id) {
		Reminder reminder = reminderRepository.findOne(id);
		if (reminder == null) {
			throw new IllegalArgumentException("Reminder doesn't exist with id=" + id);
		}

		reminder.setEmailSent(true);
		reminderRepository.save(reminder);

	}
	public void deleteAllReminder(Long id) {
		reminderRepository.deleteReminder(id);
	}

}
