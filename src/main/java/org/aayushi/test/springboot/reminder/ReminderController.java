package org.aayushi.test.springboot.reminder;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ReminderController { 

	@Autowired
	private ReminderDao reminderDao;

	@RequestMapping(method = RequestMethod.GET, value = "/reminder")
	//userId is fetched from the login 
	public ModelAndView getAllReminder(@ModelAttribute("userId") Long userId) {
	 Long	Id=userId;
		List<Reminder> allReminder = reminderDao.getAllReminder(Id);

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("home");
		modelAndView.addObject("allReminder", allReminder);
		if(!allReminder.isEmpty()) {
			modelAndView.addObject("userId", allReminder.get(0).getUser().getId());
		}
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/reminder/{id}")
	public Reminder getReminder(@PathVariable Long id) {
		return reminderDao.getReminder(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/newReminder")
	public ModelAndView newReminder(@RequestParam Long userId) {

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("newReminder");
		modelAndView.addObject("userId",userId);

		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/updateReminder") 
	public ModelAndView updateReminder(@RequestParam Long id) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("updateReminder");
		
		Reminder reminder = reminderDao.getReminder(id);
		modelAndView.addObject("reminder", reminder);
		modelAndView.addObject("userId", reminder.getUser().getId());
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/delete")
	public ModelAndView deleteReminder(@RequestParam Long id) {
		Reminder reminder = reminderDao.getReminder(id);
		if(reminder == null) {
			throw new IllegalArgumentException("Invalid reminder id : " + id);
		}
		reminderDao.deleteReminder(id);
		return buildAllReminderModelAndView(reminder.getUser().getId());
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save")
	public ModelAndView addReminder(@ModelAttribute("reminder") Reminder reminder) {
		reminderDao.addReminder(reminder);
		return buildAllReminderModelAndView(reminder.getUser().getId());
	}

	@RequestMapping(method = RequestMethod.POST, value = "/update")
	public ModelAndView update(@ModelAttribute("reminder") Reminder reminder) {
		reminderDao.updateReminder(reminder, reminder.getId());
		return buildAllReminderModelAndView(reminder.getUser().getId());
	}
	
	private ModelAndView buildAllReminderModelAndView(Long userId){
		ModelAndView modelAndView = new ModelAndView("redirect:" + "/reminder");
		modelAndView.addObject("userId", userId);
		return modelAndView;
	}

}
