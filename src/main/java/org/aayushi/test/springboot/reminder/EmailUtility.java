package org.aayushi.test.springboot.reminder;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Component;

@Component
public class EmailUtility {

	private static String USER_NAME = "styleme045@gmail.com"; // GMail user name (just the part before "@gmail.com")
	private static String PASSWORD = "Styleme*045*"; // GMail password
	

	
	public void sendEmail(String toEmail,String subject, String description ) {
		String from = USER_NAME;
		String pass = PASSWORD;
		String[] to = { toEmail }; // list of recipient email addresses
		// String subject = "Java send mail example - 2";
		// String body = "<h1>Welcome to JavaMail -2 !<h1>";

		Properties props = System.getProperties();
		// String host = "smtp.gmail.com";
		props.setProperty("mail.transport.protocol", "smtp"); 
		props.setProperty("mail.host", "smtp.gmail.com");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");

		Session session = Session.getDefaultInstance(props);
		MimeMessage message = new MimeMessage(session);

		try {
			message.setFrom(new InternetAddress(from));
			InternetAddress[] toAddress = new InternetAddress[to.length];

			// To get the array of addresses
			for (int i = 0; i < to.length; i++) {
				toAddress[i] = new InternetAddress(to[i]);
			}

			for (int i = 0; i < toAddress.length; i++) {
				message.addRecipient(Message.RecipientType.TO, toAddress[i]);
			}

			message.setSubject(subject);
			message.setContent(description, "text/html");
			

			Transport transport = session.getTransport("smtp");
			transport.connect("smtp.gmail.com", 587, from, pass);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
			System.out.println("sent email with Subject "+ subject +" , to email "+ toEmail);
		} catch (AddressException ae) {
			ae.printStackTrace();
		} catch (MessagingException me) {
			me.printStackTrace();
		}
	}
}

