package org.aayushi.test.springboot.reminder;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface ReminderRepository extends JpaRepository<Reminder, Long>{

	
	
	public List<Reminder>  findByemailSentFalse();
	
	public List<Reminder> findByUserId(Long id);
	@Modifying
	  @Transactional
	  @Query(value="delete from reminder  where user_id= ?" , nativeQuery = true)
	 public void deleteReminder(Long id);
}
