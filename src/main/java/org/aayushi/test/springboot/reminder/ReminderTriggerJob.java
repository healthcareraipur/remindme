package org.aayushi.test.springboot.reminder;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ReminderTriggerJob {

	@Autowired
	private ReminderDao reminderDao;
	@Autowired
	private EmailUtility emailUtility;

	@Scheduled(fixedDelay = 2000)
	public void checkAndSendReminder() {
		Date currdate = new Date();
		List<Reminder> reminders = reminderDao.getUnsentReminder();
		for (Reminder reminder : reminders) {
			if (reminder.getReminderDate().before(currdate)) {
				emailUtility.sendEmail(reminder.getUser().getEmail(), reminder.getSubject(), reminder.getDescription());
				reminderDao.updateEmailSent(reminder.getId());
			}
		}

	}

}
