package org.aayushi.test.springboot.user;

import java.util.ArrayList;
import java.util.List;

import org.aayushi.test.springboot.notes.NotesDao;
import org.aayushi.test.springboot.reminder.ReminderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ReminderDao reminderDao;
	@Autowired
	private NotesDao notesDao;

	public List<User> getAllUser() {

		List<User> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add);
		return users;

	}

	public User getUser(Long uid) {
		return userRepository.findOne(uid);
	}

	public void addUser(User user) {
		userRepository.save(user);
	}

	public void updateUser(User user, Long uid) {
		User findOne = userRepository.findOne(uid);
		if (findOne == null) {
			throw new IllegalArgumentException("User doesn't exist with id=" + uid);
		}
		user.setId(uid);
        userRepository.save(user);

	}
	public void deleteUser(Long uid) {
		reminderDao.deleteAllReminder(uid);
		notesDao.deleteALLNotes(uid);
		userRepository.delete(uid);

	}
	public User findByEmailAndPassword(String email , String password) {
		return userRepository.findByEmailAndPassword(email, password);
	}
	
	public User findByEmail(String email) {
	User user= userRepository.findByEmail(email);
	
	return user;
	}

}
