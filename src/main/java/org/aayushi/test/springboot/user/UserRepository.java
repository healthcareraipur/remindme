package org.aayushi.test.springboot.user;


import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long>{

	public User findByEmailAndPassword(String email , String password);
	public User findByEmail(String email);
}
