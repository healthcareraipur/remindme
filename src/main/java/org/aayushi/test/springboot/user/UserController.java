package org.aayushi.test.springboot.user;

import javax.servlet.http.HttpServletRequest;

import org.aayushi.test.springboot.otp.OTPUtility;
import org.aayushi.test.springboot.reminder.EmailUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {

	@Autowired
	UserDao userDao;
	@Autowired
	OTPUtility otpUtility;
	@Autowired
	private EmailUtility emailUtility;

	@RequestMapping(method = RequestMethod.GET, value = "/registerHere")
	public ModelAndView getRegister() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("Register");

		return modelAndView;

	}

	@RequestMapping(method = RequestMethod.GET, value = "/profileEdit")
	public ModelAndView getprofileEdit(@RequestParam Long userId) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("profileEdit");
		User user = userDao.getUser(userId);
		modelAndView.addObject("user", user);
		return modelAndView;

	}

	@RequestMapping(method = RequestMethod.POST, value = "/updateProfile")
	public ModelAndView update(@ModelAttribute("user") User user) {
		userDao.updateUser(user, user.getId());
		ModelAndView modelAndView = new ModelAndView("redirect:" + "/reminder");
		modelAndView.addObject("userId", user.getId());
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/user/{uid}")
	public User getUser(@PathVariable Long uid) {
		return userDao.getUser(uid);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/register")
	public ModelAndView addUser(@ModelAttribute("user") User user) {
		try {
			userDao.addUser(user);

		} catch (Exception e) {

			e.printStackTrace();
		}
		return new ModelAndView("redirect:" + "/loginHere");
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/user/{uid}")
	public void updateUser(@RequestBody User user, @PathVariable Long uid) {
		userDao.updateUser(user, uid);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/user/{uid}")
	public void deleteUser(@PathVariable Long uid) {
		userDao.deleteUser(uid);

	}

	@RequestMapping(method = RequestMethod.GET, value = "/loginHere")
	public ModelAndView getLogin() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");

		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/login")
	public ModelAndView checkUser(@ModelAttribute("user") User user, HttpServletRequest req) {
		User userDb = userDao.findByEmailAndPassword(user.getEmail(), user.getPassword());
		if (userDb == null) {
			return new ModelAndView("redirect:" + "/loginHere");
		} else {
			ModelAndView modelAndView = new ModelAndView("redirect:" + "/reminder");
			modelAndView.addObject("userId", userDb.getId());
			return modelAndView;
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/getEmail")
	public ModelAndView getCheckEmail() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("enterEmail");
		return modelAndView;

	}

	@RequestMapping(method = RequestMethod.POST, value = "/checkEmail")
	public ModelAndView checkEmailAndSendOTP(@ModelAttribute("email") String email) {
		User user = userDao.findByEmail(email);
		ModelAndView modelAndView = new ModelAndView();
		if (user != null) {
			String otp = otpUtility.generateOTP();
			String description = "your one time password is " + otp;
			emailUtility.sendEmail(user.getEmail(), "Verification code", description);

			modelAndView.setViewName("enterOTP");
			modelAndView.addObject("otp", otp);
			modelAndView.addObject("email", email);
			return modelAndView;
		} else {
			return new ModelAndView("redirect:" + "/getEmail");
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/checkOTP")
	public ModelAndView checkOTP(@ModelAttribute("genOTP") String otp, @ModelAttribute("enterOTP") String enterOTP,@ModelAttribute("email") String email) {
		System.out.println(otp + "  " + enterOTP);
		if (otp.equals(enterOTP)) {
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("changePassword");
			modelAndView.addObject("email", email);
			return modelAndView;
		} else {
			return new ModelAndView("redirect:" + "/getEmail");
		}

	}

	

	@RequestMapping(method = RequestMethod.POST, value = "/savePassword")
	public ModelAndView savePassword(@ModelAttribute("email") String email,@ModelAttribute("password") String password) {
		User user= userDao.findByEmail(email);
		user.setPassword(password);
		userDao.updateUser(user, user.getId());
		return new ModelAndView("redirect:" + "/loginHere");
	}
	@RequestMapping(method = RequestMethod.GET, value = "/deleteAccount")
	public ModelAndView deleteAccount(@RequestParam Long userId) {
		userDao.deleteUser(userId);
		return new ModelAndView("redirect:" + "/loginHere");
	}

}
